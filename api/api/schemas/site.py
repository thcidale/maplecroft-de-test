from api.models import Site
from api.extensions import ma, db


class SiteSchema(ma.SQLAlchemyAutoSchema):

    id = id = ma.Int()
    city = ma.String()
    country = ma.String()
    admin_area = ma.String()
    name = ma.String()
    company_id = ma.String()
    latitude = ma.String()
    longitude = ma.String()

    class Meta:
        model = Site
        sqla_session = db.session
        load_instance = True
