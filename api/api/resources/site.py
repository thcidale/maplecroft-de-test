from flask_jwt_extended import jwt_required
from flask_restful import Resource
from flask import request
from api.api.schemas.site import SiteSchema
from api.models.site import Site
from api.commons.pagination import paginate


class SiteList(Resource):

    method_decorators = [jwt_required()]

    def get(self):
        """The api should accept a query parameter `?admin_area` and list all the sites
            within this admin area

        :return:
        """
        args = request.args.to_dict()
        schema = SiteSchema(many=True)
        query = Site.query.filter_by(admin_area=args["admin_area"])
        return paginate(query, schema)
