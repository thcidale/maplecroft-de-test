import logging
import requests
import pycountry
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from api.models.site import Site

BATCH_SIZE = 10

GEOBOUNDARIES_BASE_URL = "https://www.geoboundaries.org/gbRequest.html"

BIKE_SITE_BASE_URL = "http://api.citybik.es/v2/networks"

logging.basicConfig(level=logging.INFO)


class GeoBoundariesClient:
    def __init__(self, url=None):
        self.url = url if url else GEOBOUNDARIES_BASE_URL
        self.admin_areas = ["ADM1", "ADM2", "ADM3"]

    def get_admin_area(self, latitute, longitude, country_code):
        """Get admin area from Geo Boundaries API and verify if
        the coordinates are within the polygon area."""
        country = pycountry.countries.get(alpha_2=country_code)
        if country:
            country_alpha_3 = country.alpha_3
            logging.info(f"Processing admin area for {country_alpha_3}")
            for admin_area in self.admin_areas:
                url_query = f"?ISO={country_alpha_3}&ADM={admin_area}"
                geo_url = self.url + url_query
                logging.info(f"Calling url {geo_url}")
                response = requests.get(geo_url)
                if response.json():
                    geo_boundary = self.get_boundaries(response)
                    shape_id = self.process_geo_shape(
                        geo_boundary["features"], latitute, longitude
                    )
                    if shape_id:
                        return shape_id
        else:
            logging.info(f"Country ISO code for {country_code} not found.")

    def get_boundaries(self, response):
        """Get boundaries from API."""
        dl_path = response.json()[0]["gjDownloadURL"]
        geo_boundary = requests.get(dl_path).json()
        return geo_boundary

    def process_geo_shape(self, features, latitude, longitude):
        """Process geo shape against the input coordinates."""
        for feature in features:
            if feature["geometry"]["type"] == "MultiPolygon":
                coordinates = feature["geometry"]["coordinates"]
                point = Point(latitude, longitude)
                if self.check_coordinate(coordinates, point):
                    return feature["properties"]["shapeID"]

    def check_coordinate(self, coordinates, point):
        """Check if the coordinates are within the Polygon."""
        polygon = Polygon(coordinates[0][0])
        return point.within(polygon)


class BikeSiteLoader:
    def __init__(
        self, geo_boundaries_client: GeoBoundariesClient, db_session, url: str = None
    ):
        self.url = url if url else BIKE_SITE_BASE_URL
        self.geo_boundaries_client = geo_boundaries_client
        self.db_session = db_session

    def get_bike_sites(self):
        """Call bike site sites."""
        response = requests.get(self.url)

        result = response.json()

        return result.get("networks")

    def process_bike_sites(self, bike_sites):
        """Process bike site sites."""
        logging.info(f"{len(bike_sites)} found.")
        for bike_site in bike_sites:
            bike_site_location = bike_site["location"]
            admin_area = self.geo_boundaries_client.get_admin_area(
                bike_site_location["latitude"],
                bike_site_location["longitude"],
                bike_site_location["country"],
            )
            bike_site = Site(
                city=bike_site_location["city"],
                country=bike_site_location["country"],
                admin_area=admin_area,
                name=bike_site["name"],
                company_id=bike_site["id"],
                latitude=bike_site_location["latitude"],
                longitude=bike_site_location["longitude"],
            )
            yield bike_site

    def save(self, bike_sites):
        """Save records to the database."""
        success_count = 0
        failed_count = 0
        total_count = 0
        for bike_site in bike_sites:
            try:
                total_count += 1
                self.db_session.add(bike_site)
                success_count += 1
                if total_count % BATCH_SIZE == 0:
                    logging.info(f"Saved {total_count} to the database.")
                    self.db_session.commit()
            except Exception as err:
                logging.error(err)
                failed_count += 1
        else:
            self.db_session.commit()
        return total_count, success_count, failed_count

    def run(self):
        """Run the loader process."""
        logging.info("Starting to load Bike Sites")
        bike_sites = self.get_bike_sites()
        processed_bike_sites = self.process_bike_sites(bike_sites)
        total_count, success_count, failed_count = self.save(processed_bike_sites)
        logging.info(
            f"Process finished. Total rows: {total_count}. Successful records: {success_count}. Failed records: {failed_count}."
        )
