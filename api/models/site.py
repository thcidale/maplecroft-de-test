from api.extensions import db


class Site(db.Model):
    """Basic Site model"""

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    city = db.Column(db.String, nullable=True)
    country = db.Column(db.String, nullable=True)
    admin_area = db.Column(db.String, nullable=True)
    name = db.Column(db.String, nullable=True)
    company_id = db.Column(db.String, nullable=True)
    latitude = db.Column(db.String, nullable=True)
    longitude = db.Column(db.String, nullable=True)
