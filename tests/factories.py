import factory
from api.models import User
from api.models import Site


class UserFactory(factory.Factory):

    username = factory.Sequence(lambda n: "user%d" % n)
    email = factory.Sequence(lambda n: "user%d@mail.com" % n)
    password = "mypwd"

    class Meta:
        model = User


class SiteFactory(factory.Factory):
    id = factory.Sequence(lambda n: n)
    city = factory.Sequence(lambda n: "city%d" % n)
    country = factory.Sequence(lambda n: "country%d" % n)
    admin_area = factory.Sequence(lambda n: "admin_area%d" % n)
    name = factory.Sequence(lambda n: "name%d" % n)
    company_id = factory.Sequence(lambda n: "company%d" % n)
    latitude = factory.Sequence(lambda n: "latitude%d" % n)
    longitude = factory.Sequence(lambda n: "longitude%d" % n)

    class Meta:
        model = Site
