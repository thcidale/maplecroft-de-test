import json
import pytest
from flask import url_for
from api.loader import BikeSiteLoader, GeoBoundariesClient
from api.models import Site
from unittest.mock import patch


def test_get_sites_by_admin_area(client, db, site_factory, admin_headers):
    sites_url = url_for("api.sites", admin_area="admin_area1")
    sites = site_factory.create_batch(30)

    db.session.add_all(sites)
    db.session.commit()

    rep = client.get(sites_url, headers=admin_headers)

    assert rep.status_code == 200

    results = rep.get_json()
    assert len(results["results"]) == 1
    assert results["results"][0]["id"] == 1


@pytest.fixture()
def sites_input():
    yield {
        "networks": [
            {
                "company": ["123"],
                "href": "/v2/networks/velobike-moscow",
                "id": "velobike-moscow",
                "location": {
                    "city": "Moscow",
                    "country": "RU",
                    "latitude": 55.75,
                    "longitude": 37.616667,
                },
                "name": "Velobike",
            }
        ]
    }


@pytest.fixture()
def geospacial_input():
    yield {
        "type": "FeatureCollection",
        "crs": {
            "type": "name",
            "properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"},
        },
        "features": [
            {
                "type": "Feature",
                "properties": {
                    "shapeName": "Abbey",
                    "shapeISO": "None",
                    "shapeID": "GBR-ADM3-3_0_0-B1",
                    "shapeGroup": "GBR",
                    "shapeType": "ADM3",
                },
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": [
                        [
                            [
                                [0.0855071, 51.5370432],
                                [0.0853782, 51.5369358],
                                [0.0852931, 51.5369643],
                            ]
                        ]
                    ],
                },
            }
        ],
    }


@patch("api.loader.GeoBoundariesClient")
@patch("api.loader.requests")
def test_loader_save_sites(requests_mock, geo_client_mock, db, sites_input):
    requests_mock.get.return_value.json.return_value = sites_input
    geo_client_mock.return_value.get_admin_area.return_value = "aa_1"
    bike_site_loader = BikeSiteLoader(
        geo_boundaries_client=geo_client_mock(), db_session=db.session
    )
    bike_site_loader.run()

    site = db.session.query(Site).filter_by(admin_area="aa_1").first()

    assert site.admin_area == "aa_1"


@patch("api.loader.requests")
@patch("api.loader.GeoBoundariesClient.get_boundaries")
def test_loader_geoboundary_get_admin_area(
    get_boundaries_mock, requests_mock, geospacial_input
):

    requests_mock.get.return_value.json.return_value = {"test": "test"}
    get_boundaries_mock.return_value = geospacial_input
    geoboundary_client = GeoBoundariesClient()

    admin_area = geoboundary_client.get_admin_area(0.0853928, 51.5369811, "GB")

    assert admin_area == "GBR-ADM3-3_0_0-B1"
